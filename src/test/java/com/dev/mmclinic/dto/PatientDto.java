package com.dev.mmclinic.dto;

public class PatientDto {

    private String name;
    private String ic;
    private String status;
    private int age;
    private int weight;
    private String address;
    private String gender;
    private String clinic;

    public PatientDto(String name, String ic, String status, int age, int weight, String address, String gender, String clinic) {
        this.name = name;
        this.ic = ic;
        this.status = status;
        this.age = age;
        this.weight = weight;
        this.address = address;
        this.gender = gender;
        this.clinic = clinic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }
}

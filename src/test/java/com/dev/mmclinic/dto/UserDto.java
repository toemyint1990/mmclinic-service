package com.dev.mmclinic.dto;

import com.dev.mmclinic.entity.Roles;

import java.util.List;

public class UserDto {

    private String name;
    private String samaNumber;
    private String email;
    private String password;
    private Roles role;
    private String address;
    private String mobile;
    private String clinicName;
    private List<String> icList;

    public UserDto(String name, String samaNumber, String email, String password, Roles role, String address, String mobile, String clinicName, List<String> icList) {
        this.name = name;
        this.samaNumber = samaNumber;
        this.email = email;
        this.password = password;
        this.role = role;
        this.address = address;
        this.mobile = mobile;
        this.clinicName = clinicName;
        this.icList = icList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSamaNumber() {
        return samaNumber;
    }

    public void setSamaNumber(String samaNumber) {
        this.samaNumber = samaNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public List<String> getIcList() {
        return icList;
    }

    public void setIcList(List<String> icList) {
        this.icList = icList;
    }
}

package com.dev.mmclinic.controller;

import com.dev.mmclinic.dto.ClinicDto;
import com.dev.mmclinic.dto.PatientDto;
import com.dev.mmclinic.entity.Clinic;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.repository.ClinicRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.service.ClinicService;
import com.dev.mmclinic.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class ClinicController {

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private ClinicService clinicService;

    @GetMapping("1.0/mmclinic/clinics")
    public ResponseEntity<List<Clinic>> getAllClinics() {

        List<Clinic> allClincs = clinicRepository.findAll();

        return new ResponseEntity<>(allClincs, HttpStatus.OK);
    }

    @PostMapping("1.0/mmclinic/clinic")
    public ResponseEntity<Void> createClinic(@Valid @RequestBody ClinicDto dto) {
        clinicService.createClinic(dto.getEmail()
                , dto.getAddress()
                , dto.getMobile()
                , dto.getName()
                , dto.getRegion());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}

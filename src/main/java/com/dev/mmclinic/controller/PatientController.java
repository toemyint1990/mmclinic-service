package com.dev.mmclinic.controller;

import com.dev.mmclinic.dto.PatientDto;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.entity.Status;
import com.dev.mmclinic.entity.User;
import com.dev.mmclinic.repository.MedicalRecordRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.repository.UserRepository;
import com.dev.mmclinic.service.PatientService;
import jdk.net.SocketFlow;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class PatientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);
    private static final String MMCLINIC_PATIENT_v1 = "1.0/mmclinic/patient";

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping(MMCLINIC_PATIENT_v1)
    public ResponseEntity<List<Patient>> getAllPatients() {
        List<Patient> allPatients = patientRepository.findAll();
        return new ResponseEntity<>(allPatients, HttpStatus.OK);
    }

    @GetMapping(MMCLINIC_PATIENT_v1 + "/{id}")
    public ResponseEntity<Patient> getPatientById(@PathVariable("id") Long id) {
        Patient patient = patientRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @GetMapping(MMCLINIC_PATIENT_v1 + "/{clinicId}/{email}")
    public ResponseEntity<List<Patient>> getPatientsByClinic(@PathVariable("clinicId") Long clinicId,
                                                             @PathVariable("email") String email) {

        List<Patient> patients;
        User user = userRepository.findByEmail(email);
        //below is for doctor
        if (user != null && user.getRole().roleName().equals("Doctor")) {
            patients = patientRepository.findByDoctorsIn(Arrays.asList(user));
            return new ResponseEntity<>(patients, HttpStatus.OK);
        }
        //below is for assistance
        patients = patientRepository.findByClinicIdOrderByCreatedDesc(clinicId);
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @PatchMapping(MMCLINIC_PATIENT_v1 + "/{id}")
    public ResponseEntity<?> updatePatientInfo(@PathVariable("id") Long id,
                                               @RequestBody @Valid PatientDto dto) {
        Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Patient cannot be found"));
        if (dto.getAddress() != null) patient.setAddress(dto.getAddress());
        if (dto.getName() != null) patient.setName(dto.getName());
        if (dto.getGender() != null) patient.setGender(dto.getGender());
        if (dto.getWeight() != null) patient.setWeight(dto.getWeight());
        if (dto.getStatus() != null) patient.setStatus(Status.valueOf(dto.getStatus()));
        if (dto.getMobile() != null) patient.setMobile(dto.getMobile());
        if (dto.getHeight() != null) patient.setHeight(dto.getHeight());
        patientRepository.save(patient);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping(MMCLINIC_PATIENT_v1)
    public ResponseEntity<Void> createPatient(@Valid @RequestBody PatientDto dto) {
        Patient patient = patientRepository.findByIc(dto.getIc());
        if (patient != null) {
            LOGGER.debug("Patient with IC {} already exists", dto.getIc());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        patientService.createPatient(dto.getIc(),
                dto.getName(),
                dto.getDob(),
                dto.getWeight(),
                dto.getHeight(),
                dto.getAddress(),
                dto.getMobile(),
                dto.getGender(),
                dto.getClinic());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}

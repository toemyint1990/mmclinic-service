package com.dev.mmclinic.controller;

import com.dev.mmclinic.entity.Roles;
import com.dev.mmclinic.entity.User;
import com.dev.mmclinic.dto.UserDto;
import com.dev.mmclinic.repository.UserRepository;
import com.dev.mmclinic.security.AuthResponse;
import com.dev.mmclinic.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private static final String MMCLINIC_USER_v1 = "1.0/mmclinic/user";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @GetMapping(MMCLINIC_USER_v1)
    public ResponseEntity<List<User>> getAllUsers() {

        List<User> allUser = userRepository.findAll();
        return new ResponseEntity<List<User>>(allUser, HttpStatus.OK);
    }

    @GetMapping(MMCLINIC_USER_v1 + "/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
        User user = userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(MMCLINIC_USER_v1 + "/{clinicId}/{role}")
    public ResponseEntity<List<User>> getUsersByClinic(@PathVariable("clinicId") Long id,
                                                       @PathVariable("role") String role) {
        List<User> user = userRepository.findByClinicIdAndRole(id,
                role.equals(Roles.DOCTOR)? Roles.DOCTOR : Roles.ASSISTANCE);
        return new ResponseEntity<List<User>>(user, HttpStatus.OK);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> login(@RequestBody SingInInfo singInInfo) throws Exception {
        LOGGER.info("User with email {} signing in", singInInfo.getEmail());
        AuthResponse authResponse = userService.login(singInInfo.getEmail(), singInInfo.getPassword());
        return new ResponseEntity<>(authResponse, HttpStatus.CREATED);
    }

    @PostMapping(MMCLINIC_USER_v1 + "/{userId}/{patientIdList}")
    public ResponseEntity<Void> assignDoctors(@PathVariable("userId") Long userId,
                                              @PathVariable("patientIdList") List<Long> patientIdList) {
        userService.assignDoctor(userId, patientIdList);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping(MMCLINIC_USER_v1)
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserDto userdto) throws Exception {
        Optional<User> user = userRepository.findByUsername(userdto.getUsername());
        if (user.isPresent()) {
            LOGGER.debug("User with username {} already exists", userdto.getUsername());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        userService.createUser(userdto.getName(),
                userdto.getUsername(),
                userdto.getSamaNumber(),
                userdto.getEmail(),
                userdto.getPassword(),
                userdto.getRole(),
                userdto.getAddress(),
                userdto.getMobile(),
                userdto.getClinicName(),
                userdto.getIcList());

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //Small static class for authentication purpose
    static class SingInInfo {
        @NotBlank
        String email;
        @NotBlank
        String password;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }


}

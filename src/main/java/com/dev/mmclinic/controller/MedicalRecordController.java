package com.dev.mmclinic.controller;

import com.dev.mmclinic.dto.RecordDto;
import com.dev.mmclinic.entity.MedicalRecord;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.repository.MedicalRecordRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.service.MedicalRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class MedicalRecordController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);
    private static final String MMCLINIC_RECORD_v1 = "1.0/mmclinic/record";

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    @Autowired
    private MedicalRecordService medicalRecordService;

    @GetMapping(MMCLINIC_RECORD_v1 + "/{patientId}")
    public ResponseEntity<List<MedicalRecord>> getPatientRecord(@PathVariable("patientId") Long patientId) {
        List<MedicalRecord> medicalRecords = medicalRecordRepository.findByPatientId(patientId);
        return new ResponseEntity<>(medicalRecords, HttpStatus.OK);
    }

    @PostMapping(MMCLINIC_RECORD_v1 + "/{patientId}")
    public ResponseEntity<?> createMedicalRecord(@Valid @RequestBody RecordDto dto,
                                                 @PathVariable("patientId") Long patientId) {
        medicalRecordService.createMedicalRecord(
                dto.getDiagnosis(),
                dto.getPills(),
                dto.getNote(),
                dto.getBill(), patientId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PatchMapping(MMCLINIC_RECORD_v1 + "/{id}/{patientId}")
    public ResponseEntity<?> updateMedicalRecord(@Valid @RequestBody RecordDto dto,
                                                 @PathVariable("patientId") Long patientId,
                                                 @PathVariable("id") long id) {

        Patient patient = patientRepository.findById(patientId)
                .orElseThrow(() -> new NoSuchElementException("Patient does not exist"));

        MedicalRecord record = medicalRecordRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Record does not exist"));

        if (dto.getDiagnosis() != null) record.setDiagnosis(dto.getDiagnosis());
        if (dto.getBill() != null) record.setBill(dto.getBill());
        if (dto.getNote() != null) record.setNote(dto.getNote());
        if (dto.getPills() != null) record.setPills(dto.getPills());

        MedicalRecord updatedRecord = medicalRecordRepository.save(record);
        updatedRecord.setPatient(patient);
        medicalRecordRepository.save(updatedRecord);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

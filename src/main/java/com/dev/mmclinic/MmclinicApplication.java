package com.dev.mmclinic;

import com.dev.mmclinic.entity.Clinic;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.entity.User;
import com.dev.mmclinic.repository.ClinicRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.repository.UserRepository;
import com.dev.mmclinic.service.ClinicService;
import com.dev.mmclinic.service.MedicalRecordService;
import com.dev.mmclinic.service.PatientService;
import com.dev.mmclinic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class MmclinicApplication implements CommandLineRunner {

    @Autowired
    private PatientService patientService;

    //somethin else

    @Autowired
    private ClinicService clinicService;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private MedicalRecordService medicalRecordService;

    public static void main(String[] args) {
        SpringApplication.run(MmclinicApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        createClinic();

        createPatients();

        createUsers();

        createMedicalRecord();

        retrieveInfo();


    }

    private void createClinic() {
        clinicService.createClinic("firstclinic@gmail.com",
                "firstclinic@gmail.com", "040403333", "First", "Yangon");
    }

    private void createUsers() throws Exception {
        userService.createUser("Joshua Smith", "josh", "1333-re", "smith@gmail.com", "test123",
                "DOCTOR", "Tharkayta", "04040404", "First", Arrays.asList("123", "345"));

        userService.createUser("Libral Beyonce", "beyonce", "3232-rg", "beyonce@gmail.com", "test123",
                "DOCTOR", "Tharkayta", "04040404", "First", Arrays.asList("125", "45"));

        userService.createUser("Ashlyn Brooke", "ash", null, "ashlyn@gmail.com", "test123",
                "ASSISTANCE", "Tharkayta", "04040404", "First", null);
    }

    private void createPatients() {
        patientService.createPatient("123", "John", LocalDate.of(1990,5,21), "87kg",
                "165cm", "+5523232", "Taungoo", "male", "First");
        patientService.createPatient("345", "Leslie", LocalDate.of(1991,6,22), "67kg",
                "165cm", "+5523232", "Taungoo", "Female", "First");
        patientService.createPatient("125", "Bridges", LocalDate.of(1996,4,4), "67kg",
                "165cm", "+5523232", "Taungoo", "Male", "First");
        patientService.createPatient("45", "Jones", LocalDate.of(1998,11,11), "67kg",
                "165cm", "+5523232", "Taungoo", "Female", "First");
    }

    private void createMedicalRecord() {
        medicalRecordService.createMedicalRecord("something wrong", "paracetamol, acetalyn, biogesic", "covid", 250, 1L);
        medicalRecordService.createMedicalRecord("something wrong", "paracetamol, acetalyn, biogesic", "cardiovascular", 150, 2L);
        medicalRecordService.createMedicalRecord("something wrong", "paracetamol, acetalyn, biogesic", "cardiovascular", 150, 3L);
        medicalRecordService.createMedicalRecord("something wrong", "paracetamol, acetalyn, biogesic", "heart attack", 250, 1L);
    }

    private void retrieveInfo() {
        User doctor1 = userRepository.findByEmail("smith@gmail.com");
        User doctor2 = userRepository.findByEmail("beyonce@gmail.com");
        Clinic clinic = clinicRepository.findByName("First");
        List<Patient> patientsByDoc1 = patientRepository.findByDoctorsIn(Arrays.asList(doctor1));
        List<Patient> patientsByDoc2 = patientRepository.findByDoctorsIn(Arrays.asList(doctor2));
        List<Patient> patientsByClinic = patientRepository.findByClinicIdOrderByCreatedDesc(clinic.getId());
        System.out.println("end of retrieving");
    }
}

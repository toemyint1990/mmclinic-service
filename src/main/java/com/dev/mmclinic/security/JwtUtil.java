package com.dev.mmclinic.security;

import com.dev.mmclinic.entity.Roles;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

@Component
public class JwtUtil {

    private static final String ROLE = "role";
    private static final String CLINIC = "clinic";

    // Hash value of header and payload using this secret key
    @Value("${jwt.token.secret-key}")
    String jwtSecretKey;

    @Value("${jwt.token.expiration}")
    long jwtExpirationInMilliseconds;

    public String createToken(String email, Roles role, Long clinicId) {
        //Add the username to the payload
        Claims claims = Jwts.claims().setSubject(email);
        //TODO : Convert roles to Spring Security SimpleGrantedAuthority objects,
        claims.put(ROLE, role);
        claims.put(CLINIC, clinicId);
        //Build the Token
        Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + jwtExpirationInMilliseconds))
                .signWith(SignatureAlgorithm.HS256, jwtSecretKey)
                .compact();
    }

    public boolean isValidToken(String token) {
        try {
            if (!isTokenExpired(token)) {
                Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token);
                return true;
            }
            return false;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }

    public boolean isTokenExpired (String token) {
        return getExpirationFromToken(token).before(new Date());
    }

    public Date getExpirationFromToken(String token) {
        return retrieveClaims(token, Claims::getExpiration);
    }

    public String getEmailFromToken(String token) {
        return retrieveClaims(token, Claims::getSubject);
    }

    private <T> T retrieveClaims(String token, Function<Claims,T> resolveClaimType) {
        Claims claims = Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody();
        return resolveClaimType.apply(claims);
    }

}

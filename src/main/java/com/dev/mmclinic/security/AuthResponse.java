package com.dev.mmclinic.security;

import com.dev.mmclinic.dto.UserProfileDto;

import java.io.Serializable;

public class AuthResponse implements Serializable {

    private final String token;
    private final UserProfileDto userProfile;

    public AuthResponse(String token, UserProfileDto userProfile) {
        this.token = token;
        this.userProfile = userProfile;
    }

    //Getter is to be able to retrieve from frontend
    public String getToken() {
        return token;
    }

    public UserProfileDto getUserProfile() {
        return userProfile;
    }
}

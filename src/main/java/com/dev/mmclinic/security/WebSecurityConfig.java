package com.dev.mmclinic.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String APP_AUTHENTICATE_URL = "/app/authenticate";
    private static final String REGISTER_CLINIC_URL = "/app/1.0/mmclinic/clinic";
    private static final String H2_CONSOLE_URL = "/h2-console/**";
    private static final String REGISTER_USER_URL = "/app/1.0/mmclinic/user";

    @Value("${swagger.enabled:true}")
    private boolean enableSwagger;

    @Autowired
    private JwtTokenFilter jwtTokenFilter;

    @Autowired
    private UserDetailsService userDetailsServiceImpl;

//    @Bean
//    @Override
//    public AuthenticationManager authenticationManager() throws Exception {
//        return super.authenticationManagerBean();
//    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {

        if (enableSwagger) {
            if (enableSwagger)
                web.ignoring().antMatchers(
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**");
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // for h2 console to access after implementation of authentication
        http.headers().frameOptions().disable();

        http.csrf().disable()
                .authorizeRequests()
                // only this request will allow to access without authenticated
                .antMatchers(APP_AUTHENTICATE_URL).permitAll()
                .antMatchers(REGISTER_CLINIC_URL).permitAll()
                .antMatchers(H2_CONSOLE_URL).permitAll()
                .antMatchers(HttpMethod.POST, REGISTER_USER_URL).permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                // all other requests have to be authenticated
                .anyRequest().authenticated().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

}

package com.dev.mmclinic.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Component
public class JwtTokenFilter extends GenericFilterBean {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenFilter.class);
    private static final String BEARER = "Bearer";
    private static final String AUTHORIZATION_HEADER = "Authorization";

    //This is to check jwt exist in the header or not .
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        LOGGER.info("Checking for a JSON Web token");
        String header = ((HttpServletRequest) servletRequest).getHeader(AUTHORIZATION_HEADER);
        Optional<String> token = retrieveToken(header);
        token.ifPresent(jwtToken -> {
            if (jwtUtil.isValidToken(jwtToken)) {
                // PreAuthenticatedAuthenticationToken is for role base restriction
                UserDetails userDetails = userDetailsService.loadUserByUsername(jwtUtil.getEmailFromToken(jwtToken));
                Authentication authentication =
                        new PreAuthenticatedAuthenticationToken(userDetails, "", userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        });

        // it allow request , it will go to login endpoint
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private Optional<String> retrieveToken(String header) {
        // Authorization: Bearer <token> -> value of header , optional is java 8 functionality
        if (header != null && header.startsWith(BEARER)) {
            return Optional.of(header.replace(BEARER, "").trim());
        }
        return Optional.empty();
    }
}

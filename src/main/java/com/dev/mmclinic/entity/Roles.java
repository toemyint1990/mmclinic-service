package com.dev.mmclinic.entity;

public enum Roles {
    DOCTOR("Doctor"),
    ASSISTANCE("Assistance"),
    ADMIN("Admin");

    private String name;
    Roles(String name) {
        this.name = name;
    }
    public String roleName() {
        return name;
    }
}

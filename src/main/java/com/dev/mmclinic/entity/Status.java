package com.dev.mmclinic.entity;

public enum Status {
    WAITING("Waiting"),
    CONSULTING("Consulting"),
    CONSULTED("Consulted"),
    DONE("Done");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

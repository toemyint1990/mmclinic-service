package com.dev.mmclinic.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Clinic {

    //Todo: use sequence if want to generate clinic reg number AND DUDE
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(name = "clinic_name")
    private String name;

    @Email
    @Column(name = "email")
    private String email;

    @Column(name = "region")
    private String region;

    @Column(name = "mobile")
    private String mobile;

    @Column(name= "address")
    private String address;

    @CreationTimestamp
    private LocalDateTime created;

    @UpdateTimestamp
    private LocalDateTime modified;

    @OneToMany(mappedBy = "clinic" , cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "clinic" , cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<Patient> patients = new HashSet<>();

    public Clinic() {
    }

    public Clinic (String name, String email, String region, String mobile, String address) {
        this.name = name;
        this.email = email;
        this.region = region;
        this.mobile = mobile;
        this.address = address;

    }

    public Long getId() {
        return id;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }
}

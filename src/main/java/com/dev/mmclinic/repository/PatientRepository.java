package com.dev.mmclinic.repository;

import com.dev.mmclinic.entity.Clinic;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    Patient findByIc (String ic);
    List<Patient> findByNameLike (String name);
    List<Patient> findByClinicIdOrderByCreatedDesc(Long clinicId);
    List<Patient> findByDoctorsIn(Collection<User> users);

    @Query("SELECT p FROM Patient p WHERE p.clinic.id =: clinicId AND p.status =: status")
    List<Patient> findByClinicIdAndStatus (@Param ("clinincId") Long clinicId, @Param("status") String status);




}

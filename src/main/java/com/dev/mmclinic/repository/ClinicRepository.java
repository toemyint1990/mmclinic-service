package com.dev.mmclinic.repository;

import com.dev.mmclinic.entity.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClinicRepository extends JpaRepository<Clinic, Long> {

    Clinic findByName (String name);
}

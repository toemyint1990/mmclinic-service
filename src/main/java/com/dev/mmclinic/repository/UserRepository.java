package com.dev.mmclinic.repository;

import com.dev.mmclinic.entity.Roles;
import com.dev.mmclinic.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

     User findByEmail(String email);
     Optional<User> findByUsername (String name);
     List<User> findByClinicIdAndRole(Long id, Roles role);

}

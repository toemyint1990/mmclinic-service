package com.dev.mmclinic.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class UserProfileDto implements Serializable {

    //@JsonProperty
    private final Long userId;
    //@JsonProperty
    private final Long clinicId;
    //@JsonProperty
    private final String username;
    //@JsonProperty
    private final String email;
    //@JsonProperty
    private final String address;
    //@JsonProperty
    private final String mobile;

    public Long getUserId() {
        return userId;
    }

    public Long getClinicId() {
        return clinicId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getMobile() {
        return mobile;
    }

    public UserProfileDto(Long userId, Long clinicId, String username, String email, String address, String mobile) {
        this.userId = userId;
        this.clinicId = clinicId;
        this.username = username;
        this.email = email;
        this.address = address;
        this.mobile = mobile;
    }

    public static class Builder {

        private Long userId;
        private Long clinicId;
        private String username;
        private String email;
        private String address;
        private String mobile;

        public Builder() {

        }

        public Builder setUserId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder setClinicId(Long clinicId) {
            this.clinicId = clinicId;
            return this;
        }

        public Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setMobile(String mobile) {
            this.mobile = mobile;
            return this;
        }

        public UserProfileDto build() {
            return new UserProfileDto(userId, clinicId, username, email, address, mobile);
        }
    }
}

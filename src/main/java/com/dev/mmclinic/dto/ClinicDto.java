package com.dev.mmclinic.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class ClinicDto {

    @NotBlank
    private String name;
    @Email
    private String email;
    private String address;
    private String mobile;
    private String region;

    public ClinicDto(String name, String email, String address, String mobile, String region) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.mobile = mobile;
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}

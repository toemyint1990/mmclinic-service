package com.dev.mmclinic.dto;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

public class PatientDto {

    @NotBlank
    private String name;
    @NotBlank
    private String ic;
    private String status;
    private LocalDate dob;
    private String weight;
    private String height;
    private String address;
    private String gender;
    private String mobile;
    @NotBlank
    private String clinic;

    public PatientDto(String name, String ic, String status, LocalDate dob, String weight, String height, String address, String gender, String mobile, String clinic) {
        this.name = name;
        this.mobile = mobile;
        this.ic = ic;
        this.status = status;
        this.dob = dob;
        this.weight = weight;
        this.height = height;
        this.address = address;
        this.gender = gender;
        this.clinic = clinic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIc() {
        return ic;
    }

    public void setIc(String ic) {
        this.ic = ic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }
}

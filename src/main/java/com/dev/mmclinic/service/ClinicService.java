package com.dev.mmclinic.service;

import com.dev.mmclinic.entity.Clinic;
import com.dev.mmclinic.repository.ClinicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClinicService {

    @Autowired
    private ClinicRepository clinicRepository;

    public void createClinic (String email,
                        String address,
                        String mobile,
                        String name,
                        String region) {

        clinicRepository.save(new Clinic(name , email, region, mobile, address));
    }
}

package com.dev.mmclinic.service;

import com.dev.mmclinic.entity.Clinic;
import com.dev.mmclinic.entity.Patient;
import com.dev.mmclinic.entity.Status;
import com.dev.mmclinic.repository.ClinicRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class PatientService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private PatientRepository patientRepository;

    public void createPatient (String ic,
                               String name,
                               LocalDate dob,
                               String weight,
                               String height,
                               String mobile,
                               String address,
                               String gender,
                               String clinicName) {

        patientRepository.save(new Patient(ic, name, Status.WAITING, dob, weight, height, address, mobile, gender));

        Clinic clinic = clinicRepository.findByName(clinicName);
        Patient p = patientRepository.findByIc(ic);
        p.setClinic(clinic);
        patientRepository.save(p);
    }

    public Long totalPatient() {
        return patientRepository.count();
    }
}

package com.dev.mmclinic.service;

import com.dev.mmclinic.entity.MedicalRecord;
import com.dev.mmclinic.repository.MedicalRecordRepository;
import com.dev.mmclinic.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicalRecordService {

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    @Autowired
    private PatientRepository patientRepository;

    public void createMedicalRecord (String diagnosis,
                        String pills,
                        String note,
                        int bill,
                        Long patientId) {

        MedicalRecord record = medicalRecordRepository.save(new MedicalRecord(diagnosis, pills, note, bill));

        patientRepository.findById(patientId).ifPresent(patient -> {
            record.setPatient(patient);
            medicalRecordRepository.save(record);
        });
    }
}

package com.dev.mmclinic.service;

import com.dev.mmclinic.dto.UserProfileDto;
import com.dev.mmclinic.entity.*;
import com.dev.mmclinic.repository.ClinicRepository;
import com.dev.mmclinic.repository.PatientRepository;
import com.dev.mmclinic.repository.UserRepository;
import com.dev.mmclinic.security.AuthResponse;
import com.dev.mmclinic.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private ClinicRepository clinicRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void createUser(String name,
                           String username,
                           String sama_number,
                           String email,
                           String password,
                           String role,
                           String address,
                           String mobile,
                           String clinicName,
                           List<String> icList) throws Exception {

        userRepository.save(new User(name, username, sama_number, email, passwordEncoder.encode(password), mobile,
                Roles.valueOf(role), address));

        Clinic clinic = clinicRepository.findByName(clinicName);
        Set<Patient> patients = new HashSet<>();

        if (icList != null && icList.size() > 0)
            icList.forEach( ic -> patients.add(patientRepository.findByIc(ic)));

        User newUser = userRepository.findByEmail(email);
            newUser.setClinic(clinic);
            newUser.setPatients(patients);
            userRepository.save(newUser);

    }

    public void assignDoctor (Long doctorId, List<Long> patientIdList) {
        Optional<User> user = userRepository.findById(doctorId);
        user.ifPresent(doc -> {
            Set<Patient> patients = new HashSet<>();
            if (patientIdList != null && patientIdList.size() > 0)
                patientIdList.forEach(id ->{
                        Patient  patient=  patientRepository.findById(id).get();
                        patient.setStatus(Status.WAITING);
                        patients.add(patient);
                });
            doc.setPatients(patients);
        } );
    }

    public AuthResponse login(String email, String password) throws Exception {
        User user = userRepository.findByEmail(email);
        if (user == null)
            throw new Exception("User does not exist");
        Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        String token = jwtUtil.createToken(email,user.getRole(), user.getClinic().getId());
        UserProfileDto userProfile = new UserProfileDto.Builder()
                .setUserId(user.getId())
                .setClinicId(user.getClinic().getId())
                .setEmail(user.getEmail())
                .setAddress(user.getAddress())
                .setUsername(user.getUsername())
                .setMobile(user.getMobile())
                .build();

        return new AuthResponse(token, userProfile);
    }

    public Long totalUsers() {
        return userRepository.count();
    }
}
